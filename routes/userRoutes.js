
const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	registerUser,
	verifyEmail,
	loginUser,
	userDetails,
	changePassword,
	editProfile,
	allUsers,
	userStatus
} = require('../controllers/userController');


// user registration
router.post("/register", registerUser );

//verify user email
router.post("/verifyEmail",verifyEmail);

// user login
router.post("/login", loginUser);


//get user details  (users and admin)
router.get("/details", auth.verify, userDetails);
router.get("/all", auth.verify, allUsers);


router.patch("/changePassword", auth.verify, changePassword) 
//edit profile
router.put("/details/:userId", auth.verify, editProfile);
//update user status (admin)
router.patch("/details/:userId", auth.verify, userStatus);



module.exports = router;
