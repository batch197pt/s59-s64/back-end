const express = require('express');
const router = express.Router();
const auth = require('../auth');
const {
	addProduct,
	getAllProduct,
	viewProducts,
	getProduct,
	updateProduct,
	archiveProduct,
	unArchiveProduct
} = require('../controllers/productController');
 


//create product / get all products
router.route("/").post(auth.verify, addProduct).get(getAllProduct);
router.route("/viewProducts").get(viewProducts);

//get specific product and update product
router.route("/:productId").get(getProduct).put(auth.verify, updateProduct);


//archive route
router.patch("/:productId/archive", auth.verify, archiveProduct)
router.patch("/:productId/unarchive", auth.verify, unArchiveProduct)





module.exports= router;