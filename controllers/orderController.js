const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');

const orderProduct = async (req, res) => {
    try {

        const verifiedUser = await auth.decode(req.headers.authorization);

        if (verifiedUser.isAdmin) {
            return res.send('You are an Admin');
        }

        const productsArr = req.body.products

    

        //get total amount
        const total = productsArr.map((product) => product.subtotal).reduce((val, currentVal) => val + currentVal);
        

        //order schema
        const newOrder = new Order({
            userId: verifiedUser.id,
            products: productsArr,
            totalAmount: total
        })

        const save = await newOrder.save();

        //update product count, productCount - order quantity
        for (let i = 0; i < productsArr.length; i++) {
            let id =productsArr[i].productId;
            let quantity = productsArr[i].quantity;
            const editProduct= await Product.findByIdAndUpdate(id, { $inc: { quantity: -quantity } });
            
        }

        return res.send(true);


    } catch (error) {
        return res.send(error);
    }

};


//ordered products
const orderedProduct = async (req, res) => {
    try {

        const verifiedUser = await auth.decode(req.headers.authorization);

        if (verifiedUser.isAdmin) {
            const getUsersAllOrder = await Order.find({}, { __v: 0 });
            return res.send(getUsersAllOrder);
        }

        const getAllOrder = await Order.find({ userId: verifiedUser.id });
        return res.send(getAllOrder)

    } catch (error) {
        return res.send(error.message);
    }
};


module.exports = {
    orderProduct,
    orderedProduct
}