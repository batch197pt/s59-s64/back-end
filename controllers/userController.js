const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// registration
const registerUser = async (req, res) => {
    try {

        let newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 10),
            mobileNo: req.body.mobileNo,
            address: req.body.address
        });

        const result = await newUser.save();
        return res.send(true);


    } catch (error) {
      
       return res.send(error);
    }

};


//verify user email if already exist in the data base

const verifyEmail = async(req,res)=>{
    try{

      const findEmail = await User.findOne({email:req.body.email});
        
        if(findEmail > 0){
            return res.send(true)
        } else {
            return  res.send(false)
        }

    } catch (error){
        return res.send(error);
    }
};


//login user
const loginUser = async (req, res) => {
    try {
        const findUser = await User.findOne({ email: req.body.email });
        if (findUser==null) {
            return res.send(false)
        }
        const isPasswordCorrect = await bcrypt.compareSync(req.body.password, findUser.password);
        if (isPasswordCorrect) {
            return res.send({ access: auth.createAccessToken(findUser)})
        } else {
            return res.send(false)
        }

    } catch (error) {
        return res.send(error)
    }
};


//get user information/details
const userDetails = async (req, res ) => {
	try{
	  	const verifiedUser = await auth.decode(req.headers.authorization);
	  	
        if(!verifiedUser){
            return res.send(false);
        }
         const data = await User.findById(verifiedUser.id);
            return res.send(data);

	} catch (error) {
		return res.send(error)
	}
};


const changePassword = async (req, res) => {
    try{

        const verifiedUser = await auth.decode(req.headers.authorization);
            if(!verifiedUser){
                return res.send(false);
            }

        const findUser = await User.findOne({ _id:verifiedUser.id });
            if (findUser==null) {
            return res.send(false)
            }

        const isPasswordCorrect = await bcrypt.compareSync(req.body.password, findUser.password);
            if (isPasswordCorrect) {
                const edit = await User.findByIdAndUpdate(findUser._id, {password: bcrypt.hashSync(req.body.newPassword, 10)});
                    return res.send(true);
            } else {
                return res.send(false)
            }


    } catch (error){
        return res.send(error)
    }
};



const editProfile = async (req, res ) => {
    try{
        const verifiedUser = await auth.decode(req.headers.authorization);
        
        if(!verifiedUser){
            return res.send(false);
        }
            const edit = await User.findByIdAndUpdate(req.params.userId, req.body);
            return res.send(true);

    } catch (error) {
        return res.send(error)
    }
};

//get all users
const allUsers = async (req, res)=> {
    try{
        const verifiedUser = await auth.decode(req.headers.authorization);
        
        if(!verifiedUser.isAdmin){
            return res.send(false);     
        }

        const getUsers = await User.find({ _id : { $ne : verifiedUser.id }}); 
        return res.send(getUsers);


    } catch (error) {
        return res.send(error.message)
    }
};


//update user status 
const userStatus = async (req, res)=> {
    try{
        const verifiedUser = await auth.decode(req.headers.authorization);
        
        if(!verifiedUser.isAdmin){
            return res.send(false);     
        }

        const statusUpdate = await User.findByIdAndUpdate(req.params.userId,{isAdmin:req.body.isAdmin}); 
        return res.send(true);

    } catch (error) {
        return res.send(error.message)
    }
};


module.exports = {
	registerUser,
    verifyEmail,
	loginUser,
    userDetails,
    changePassword,
    editProfile,
    allUsers,
    userStatus
}