const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required : [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required : [true, "Last Name is required"]
	},
	email:{
		type: String,
		required : [true, "E-mail is required"],
		unique:true
	},
	password:{
		type: String,
		required : [true, "password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true,"Mobile number is required"]
	},
	address:{
		type:String,
		required: [true,"address is required"]
	}
});


module.exports = mongoose.model("User", userSchema);