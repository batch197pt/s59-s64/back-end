const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

    userId: {
        type: String,
        required: [true, "user ID is required"]
    },
    products: {
        type: [new mongoose.Schema({
            productId: {
                type: String,
                required: [true, "Product Id is required"]
            },
             productName: {
                type: String,
                required: [true, "Product Name is required"]
            },
             imageUrl: {
                type: String,
                required: [true, "Product imageUrl is required"]
            },
            price:{
                type: Number,
                required: [true, "Product price is required"]
            },
            quantity: {
                type: Number,
                default: 1
            },
            subtotal:{
                 type: Number,
                 default: 0
            }
        })]
    },
    totalAmount: {
        type: Number,
        required: [true, "total amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: Date.now
    }

});



module.exports = mongoose.model("Order", orderSchema)


// ;