const express = require('express');
const cors = require('cors');


// //mongoDb connection
const connectDB = require('./connectDb/dbConnection');



// routes
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();
const port = process.env.PORT || 5000;


// middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// main uri
app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes)



const start = async () => {
    try {
        await connectDB();
        app.listen(port, () => console.log(`Server is listening on port ${port}`));
    } catch (error) {
        console.log(error);
    }
};

start();